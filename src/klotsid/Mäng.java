package klotsid;

import java.awt.event.KeyListener;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.Timer;

import javax.swing.JPanel;

public class M�ng extends JPanel implements KeyListener, ActionListener {
	
	private boolean m�ng = false;
	private int punktid = 0;

	private int klotsideArv = 18;
	
	private Timer timer;
	private int delay = 6;

	private int m�ngija = 310;

	private int pallX = 120;
	private int pallY = 350;
	private int pallXsuund = -2;
	private int pallYsuund = -2;
	
	private LevelGeneraator level;

	public M�ng() {
		level = new LevelGeneraator(3, 6);
		addKeyListener(this);
		setFocusable(true);
		timer = new Timer(delay, this);
		timer.start();

	}

	public void paint(Graphics g) {
		// taust
		g.setColor(Color.gray);
		g.fillRect(1, 1, 692, 592);
		
		//level
		level.draw((Graphics2D)g);
		
		// ��rised
		g.setColor(Color.gray);
		g.fillRect(0, 0, 2, 590);
		g.fillRect(0, 0, 690, 2);
		g.fillRect(690, 0, 2, 590);
		
		//punktid
		g.setColor(Color.BLACK);
		g.setFont(new Font("impact", Font.BOLD, 25));
		g.drawString("Skoor: "+punktid, 570, 30);

		// p�rkelaud
		g.setColor(Color.red);
		g.fillRect(m�ngija, 550, 100, 20);

		// pall
		g.setColor(Color.orange);
		g.fillOval(pallX, pallY, 25, 25);
		
		if(klotsideArv <= 0) {
			m�ng = false;
			pallXsuund = 0;
			pallYsuund = 0;
			g.setColor(Color.white);
			g.setFont(new Font("impact", Font.BOLD, 29));
			g.drawString("V�itsid m�ngu!", 250, 295);
			
			g.setFont(new Font("impact", Font.BOLD, 20));
			g.drawString("Vajuta Enter, et uuesti m�ngida.", 215, 345);
			
		}
		if(pallY > 570) {
			m�ng = false;
			pallXsuund = 0;
			pallYsuund = 0;
			g.setColor(Color.black);
			g.setFont(new Font("impact", Font.BOLD, 29));
			g.drawString("M�ng l�bi! Skoor: " + punktid, 220, 295);
			
			g.setFont(new Font("arial", Font.BOLD, 20));
			g.drawString("Vajuta Enter, et uuesti proovida.", 215, 345);
		}
		
	}
	
	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void actionPerformed(ActionEvent e) {
		timer.start();
		if(m�ng) {
			if(new Rectangle(pallX, pallY, 20, 20).intersects(new Rectangle(m�ngija, 550, 100, 8))) {
				pallYsuund = -pallYsuund;
			}
			
			for(int i = 0; i<level.level.length ; i++) {                    //https://youtu.be/K9qMm3JbOH0?t=2006
				for(int j= 0; j<level.level[0].length; j++){
					if(level.level[i][j] > 0) {
						int klotsX = j* level.klotsiLaius + 80;
						int klotsY = i * level.klotsiK�rgus + 50;
						int klotsiLaius = level.klotsiLaius;
						int klotsiK�rgus = level.klotsiK�rgus;
						
						Rectangle rect = new Rectangle(klotsX, klotsY, klotsiLaius, klotsiK�rgus);
						Rectangle pallirect = new Rectangle(pallX, pallY, 20, 20);
						
						
						if(pallirect.intersects(rect)) {
							level.klotsiV��rtus(0, i, j);
							klotsideArv--;
							punktid += 10;
							
							if(pallX <= rect.x || pallX  >= rect.x + rect.width) {
								pallXsuund = -pallXsuund;
							} else {
								pallYsuund = -pallYsuund;
							}
							
						}
						
					}
				}
			}
			
			pallX += pallXsuund;
			pallY += pallYsuund;
			if(pallX < 0) {
				pallXsuund = -pallXsuund;
			}
			if(pallY < 0) {
				pallYsuund = -pallYsuund;
			}
			if(pallX > 670) {
				pallXsuund = -pallXsuund;
			}
		}
		
		repaint();

	}
	
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (m�ngija >= 600) {
				m�ngija = 600;
			} else {
				liiguParemale();
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			if (m�ngija < 5) {
				m�ngija = 5;
			} else {
				liiguVasakule();
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			if(!m�ng) {
				m�ng = true;
				pallX = 120;
				pallY = 350;
				pallXsuund = -2;
				pallYsuund = -2;
				m�ngija = 310;
				punktid = 0;
				klotsideArv = 18;
				level = new LevelGeneraator(3, 6);
				
				repaint();
			}
		}

	}

	

	public void liiguParemale() {
		m�ng = true;
		m�ngija += 25;
	}

	public void liiguVasakule() {
		m�ng = true;
		m�ngija -= 25;
	}
}

