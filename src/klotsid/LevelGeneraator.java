package klotsid;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class LevelGeneraator {
	public int level[][];
	public int klotsiLaius;
	public int klotsiKõrgus;

	public LevelGeneraator(int row, int col) {
		level = new int[row][col];
		for (int i = 0; i < level.length; i++) {            //https://youtu.be/K9qMm3JbOH0?t=2006
			for (int j = 0; j < level[0].length; j++) {
				level[i][j] = 1;
			}
		}

		klotsiLaius = 540 / col;
		klotsiKõrgus = 150 / row;
	}

	public void draw(Graphics2D g) {
		for (int i = 0; i < level.length; i++) {
			for (int j = 0; j < level[0].length; j++) {
				if(level[i][j] > 0) {
					g.setColor(Color.cyan);
					g.fillRect(j * klotsiLaius + 80, i * klotsiKõrgus + 50, klotsiLaius, klotsiKõrgus);
					
					g.setStroke(new BasicStroke(5));
					g.setColor(Color.gray);
					g.drawRect(j * klotsiLaius + 80, i * klotsiKõrgus + 50, klotsiLaius, klotsiKõrgus);
				}
			}
		}

	}
	public void klotsiVäärtus(int value, int row, int col) {
		level[row][col] = value;
	}
}
